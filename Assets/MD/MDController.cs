﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Collections.Generic;

public class MDController : MonoBehaviour {

    public static string type = "Organic";

    public Atom atomPrefab;
    public Bond bondPrefab;

	public Atom[] atoms;

	private float sigma6;
	private float sigma12;

    private EAM cu_eam;
    private EAM fe_eam;
    private Amber amber;

    private Potential pot;

    public Bond[] bonds;
    public Angle[] angles;
    public Dihedral[] dihedrals;

    public Camera camera;

    public bool damping = false;

    private Dictionary<string, Color> atomColors;
    private Dictionary<string, float> atomSizes;
    private Dictionary<string, float> atomMasses;
    private Dictionary<string, float> atomEps;
    private Dictionary<string, float> atomR;

    private float vdWScale = 0.3f;
    private float stiffnessScale = 0.005f;

	// Use this for initialization
	void Start() {
        cu_eam = new EAM(this, "Assets/Resources/Cu_mishin1.eam.alloy");
        fe_eam = new EAM(this, "Assets/Resources/Fe_Mishin2006.eam.alloy");
        amber = new Amber(this);

        switch (type) {
            case "Organic":
                pot = amber;
                CreateDictionaries();
                break;
            case "Copper":
                pot = cu_eam;
                break;
            case "Iron":
                pot = fe_eam;
                break;
        }
        GenerateAtoms();
    }

    private void CreateDictionaries()
    {
        atomColors = new Dictionary<string, Color>();
        atomColors.Add("H", Color.white);
        atomColors.Add("C", Color.black);
        atomColors.Add("O", Color.red);
        atomColors.Add("N", Color.blue);

        atomMasses = new Dictionary<string, float>();
        atomMasses.Add("H", 1.008f);
        atomMasses.Add("C", 12.011f);
        atomMasses.Add("O", 15.9994f);
        atomMasses.Add("N", 14.007f);

        atomSizes = new Dictionary<string, float>();
        atomSizes.Add("H", 0.12f * vdWScale);
        atomSizes.Add("C", 0.17f * vdWScale);
        atomSizes.Add("O", 0.152f * vdWScale);
        atomSizes.Add("N", 0.155f * vdWScale);

        atomEps = new Dictionary<string, float>();
        atomEps.Add("H", -0.046f);
        atomEps.Add("C", -0.032f);
        atomEps.Add("O", -0.12f);
        atomEps.Add("N", -0.2f);

        atomR = new Dictionary<string, float>();
        atomR.Add("H", 1.34f/2);
        atomR.Add("C", 2.0f/2);
        atomR.Add("O", 1.7f/2);
        atomR.Add("N", 1.85f / 2);
    }

    private void GenerateAtoms()
    {
        switch (type) {
            case "Organic":
                Organic();
                break;
            case "Copper":
                Copper();
                break;
            case "Iron":
                Iron();
                break;
        }
    }

    private void ReadStructure(string file)
    {
        StreamReader reader = new StreamReader(file);
        char[] sep = new char[] { ' ' };
        reader.ReadLine();
        reader.ReadLine();
        reader.ReadLine();
        string[] tokens;
        tokens = reader.ReadLine().Split(sep, System.StringSplitOptions.RemoveEmptyEntries);
        int numAtoms = int.Parse(tokens[0]);
        int numBonds = int.Parse(tokens[1]);

        atoms = new Atom[numAtoms];
        bonds = new Bond[numBonds];

        for(int i = 0; i < numAtoms; i++)
        {
            tokens = reader.ReadLine().Split(sep, System.StringSplitOptions.RemoveEmptyEntries);
            float x = float.Parse(tokens[0])/10;
            float y = float.Parse(tokens[1])/10;
            float z = float.Parse(tokens[2])/10;
            string type = tokens[3];
            Atom atom = Instantiate<Atom>(atomPrefab, new Vector3(x, y, z), Quaternion.identity, transform);
            atom.type = type;

            atom.color = atomColors[type];
            atom.mass = atomMasses[type];
            atom.eps = atomEps[type] * stiffnessScale;
            atom.r = atomR[type];
            atom.diameter = 2 * atomSizes[type];

            atoms[i] = atom;
        }
        for(int i = 0; i < numBonds; i++)
        {
            tokens = reader.ReadLine().Split(sep, System.StringSplitOptions.RemoveEmptyEntries);
            int atom1 = int.Parse(tokens[0])-1;
            int atom2 = int.Parse(tokens[1])-1;
            int order = int.Parse(tokens[2]);
            atoms[atom1].bonds.Add(atom2);
            atoms[atom2].bonds.Add(atom1);
            Bond bond = Instantiate<Bond>(bondPrefab, transform);
            bond.atom1 = atoms[atom1];
            bond.atom2 = atoms[atom2];
            bond.camera = camera;
            bond.order = order;

            string types = bond.atom1.type + bond.atom2.type;
            switch (types)
            {
                case "CH":
                case "HC":
                    bond.length = 1.111f;
                    bond.stiffness = 309.0f * stiffnessScale;
                    break;
                case "OH":
                case "HO":
                    bond.length = 0.96f;
                    bond.stiffness = 545.0f * stiffnessScale;
                    break;
                case "CO":
                case "OC":
                    if(bond.order == 1)
                    {
                        bond.length = 1.42f;
                        bond.stiffness = 428.0f * stiffnessScale;
                    }
                    else
                    {
                        bond.length = 1.23f;
                        bond.stiffness = 620.0f * stiffnessScale;
                    }
                    break;
                case "CC":
                    if (bond.order == 1)
                    {
                        bond.length = 1.5f;
                        bond.stiffness = 222.5f * stiffnessScale;
                    } else
                    {
                        bond.length = 1.34f;
                        bond.stiffness = 440.0f * stiffnessScale;
                    }
                    break;
                case "NC":
                case "CN":
                    if(bond.order == 1)
                    {
                        bond.length = 1.36f;
                        bond.stiffness = 430.0f * stiffnessScale;
                    } else
                    {
                        bond.length = 1.345f;
                        bond.stiffness = 370.0f * stiffnessScale;
                    }
                    break;
                case "NH":
                case "HN":
                    bond.length = 1.0f;
                    bond.stiffness = 480.0f * stiffnessScale;
                    break;
                default:
                    bond.length = 0;
                    bond.stiffness = 0;
                    break;
            } 

            bonds[i] = bond;
        }
        List<Angle> angles = new List<Angle>();
        for(int a =0;a<numAtoms;a++)
        {
            for(int i = 0; i < atoms[a].bonds.Count; i++)
            {
                for(int j = i + 1; j < atoms[a].bonds.Count; j++) {
                    Angle angle = new Angle();
                    angle.atom1 = atoms[a];
                    angle.atom2 = atoms[atoms[a].bonds[i]];
                    angle.atom3 = atoms[atoms[a].bonds[j]];

                    string types = angle.atom1.type + angle.atom2.type + angle.atom3.type;
                    switch (types)
                    {
                        case "CCC":
                            angle.theta = 113.5f;
                            angle.stiffness = 53.35f * stiffnessScale;
                            break;
                        case "CCH":
                        case "CHC":
                            angle.theta = 110.1f;
                            angle.stiffness = 34.5f * stiffnessScale;
                            break;
                        case "COC":
                        case "CCO":
                            angle.theta = 110.1f;
                            angle.stiffness = 75.70f * stiffnessScale;
                            break;
                        case "CHH":
                            angle.theta = 109.0f;
                            angle.stiffness = 35.5f*stiffnessScale;
                            break;
                        case "CHO":
                        case "COH":
                            angle.theta = 108.89f;
                            angle.stiffness = 55.0f * stiffnessScale;
                            break;
                        case "OCH":
                        case "OHC":
                            angle.theta = 109.0f;
                            angle.stiffness = 50.0f * stiffnessScale;
                            break;
                        case "NCC":
                            angle.theta = 120.0f;
                            angle.stiffness = 50.0f * stiffnessScale;
                            break;
                        case "CNO":
                        case "CON":
                            angle.theta = 122.5f;
                            angle.stiffness = 80.0f * stiffnessScale;
                            break;
                        case "CNN":
                            angle.theta = 112.5f;
                            angle.stiffness = 130.0f * stiffnessScale;
                            break;
                        case "CNC":
                        case "CCN":
                            angle.theta = 116.5f;
                            angle.stiffness = 80.0f * stiffnessScale;
                            break;
                        case "CNH":
                        case "CHN":
                            angle.theta = 108.0f;
                            angle.stiffness = 48.0f * stiffnessScale;
                            break;
                        case "NHC":
                        case "NCH":
                            angle.theta = 109.5f;
                            angle.stiffness = 30.0f * stiffnessScale;
                            break;
                        case "OCC":
                            angle.theta = 109.7f;
                            angle.stiffness = 95.0f * stiffnessScale;
                            break;
                        case "COO":
                            angle.theta = 112.0f;
                            angle.stiffness = 90.0f * stiffnessScale;
                            break;
                        default:
                            angle.theta = 0;
                            angle.stiffness = 0;
                            break;
                    }
                    angles.Add(angle);
                }
            }
        }
        this.angles = angles.ToArray();

        List<Dihedral> dihedrals = new List<Dihedral>();
        foreach(var bond in bonds)
        {
            foreach(int i in bond.atom1.bonds)
            {
                foreach(int j in bond.atom2.bonds)
                {
                    Dihedral dihedral = new Dihedral();
                    dihedral.atom1 = atoms[i];
                    dihedral.atom2 = bond.atom1;
                    dihedral.atom3 = bond.atom2;
                    dihedral.atom4 = atoms[j];

                    string types = dihedral.atom1.type + dihedral.atom2.type + dihedral.atom3.type + dihedral.atom4.type;
                    switch (types)
                    {
                        case "CCCC":
                            dihedral.k.Add(0.02f*stiffnessScale);
                            dihedral.k.Add(0.06f*stiffnessScale);
                            dihedral.k.Add(2.47f*stiffnessScale);
                            dihedral.delta.Add(180);
                            dihedral.delta.Add(0);
                            dihedral.delta.Add(0);
                            break;
                        case "HCCH":
                            dihedral.k.Add(0);
                            dihedral.k.Add(0);
                            dihedral.k.Add(0.2f * stiffnessScale);
                            dihedral.delta.Add(0);
                            dihedral.delta.Add(0);
                            dihedral.delta.Add(0);
                            break;
                        case "OCCO":
                            dihedral.k.Add(0.14f * stiffnessScale);
                            dihedral.k.Add(0.7f * stiffnessScale);
                            dihedral.k.Add(0.18f * stiffnessScale);
                            dihedral.delta.Add(0);
                            dihedral.delta.Add(0);
                            dihedral.delta.Add(0);
                            break;
                        case "NCCN":
                            dihedral.k.Add(0.3f * stiffnessScale);
                            dihedral.k.Add(0);
                            dihedral.k.Add(0);
                            dihedral.k.Add(-0.3f*stiffnessScale);
                            dihedral.delta.Add(0);
                            dihedral.delta.Add(0);
                            dihedral.delta.Add(0);
                            dihedral.delta.Add(0);
                            break;
                        case "HCCC":
                        case "CCCH":
                            dihedral.k.Add(0);
                            dihedral.k.Add(0);
                            dihedral.k.Add(0.2f * stiffnessScale);
                            dihedral.delta.Add(0);
                            dihedral.delta.Add(0);
                            dihedral.delta.Add(0);
                            break;
                        case "HCCO":
                        case "OCCH":
                            dihedral.k.Add(0);
                            dihedral.k.Add(0);
                            dihedral.k.Add(0.14f * stiffnessScale);
                            dihedral.delta.Add(0);
                            dihedral.delta.Add(0);
                            dihedral.delta.Add(0);
                            break;
                        case "HCCN":
                        case "NCCH":
                            dihedral.k.Add(0.4f * stiffnessScale);
                            dihedral.k.Add(0.6f * stiffnessScale);
                            dihedral.delta.Add(180f);
                            dihedral.delta.Add(0);
                            break;
                        case "OCCN":
                        case "NCCO":
                            dihedral.k.Add(0);
                            dihedral.k.Add(0);
                            dihedral.k.Add(0.16f * stiffnessScale);
                            dihedral.delta.Add(0);
                            dihedral.delta.Add(0);
                            dihedral.delta.Add(0);
                            break;

                        case "CCOC":
                        case "COCC":
                            dihedral.k.Add(2.58f * stiffnessScale);
                            dihedral.k.Add(0.24f * stiffnessScale);
                            dihedral.k.Add(0.36f * stiffnessScale);
                            dihedral.delta.Add(0);
                            dihedral.delta.Add(180f);
                            dihedral.delta.Add(180f);
                            break;
                        case "HCOH":
                        case "HOCH":
                            dihedral.k.Add(0);
                            dihedral.k.Add(0);
                            dihedral.k.Add(0.18f * stiffnessScale);
                            dihedral.delta.Add(0);
                            dihedral.delta.Add(0);
                            dihedral.delta.Add(0);
                            break;
                        case "HCOC":
                        case "COCH":
                            dihedral.k.Add(0);
                            dihedral.k.Add(0);
                            dihedral.k.Add(0.3f * stiffnessScale);
                            dihedral.delta.Add(0);
                            dihedral.delta.Add(0);
                            dihedral.delta.Add(0);
                            break;
                        case "CCOH":
                        case "HOCC":
                            dihedral.k.Add(0.59f * stiffnessScale);
                            dihedral.k.Add(0.38f * stiffnessScale);
                            dihedral.k.Add(0.13f * stiffnessScale);
                            dihedral.delta.Add(0);
                            dihedral.delta.Add(0);
                            dihedral.delta.Add(0);
                            break;
                        case "OCOC":
                        case "COCO":
                            dihedral.k.Add(0.76f * stiffnessScale);
                            dihedral.k.Add(1.25f * stiffnessScale);
                            dihedral.k.Add(0.48f * stiffnessScale);
                            dihedral.delta.Add(180f);
                            dihedral.delta.Add(0);
                            dihedral.delta.Add(180f);
                            break;
                        case "OCOH":
                        case "HOCO":
                            dihedral.k.Add(0.90f * stiffnessScale);
                            dihedral.k.Add(1.14f * stiffnessScale);
                            dihedral.k.Add(0.11f * stiffnessScale);
                            dihedral.delta.Add(0);
                            dihedral.delta.Add(0);
                            dihedral.delta.Add(180f);
                            break;
                        case "NCOH":
                        case "HOCN":
                            dihedral.k.Add(0);
                            dihedral.k.Add(0);
                            dihedral.k.Add(0.14f * stiffnessScale);
                            dihedral.delta.Add(0);
                            dihedral.delta.Add(0);
                            dihedral.delta.Add(0);
                            break;
                        case "NCOC":
                        case "COCN":
                            dihedral.k.Add(0);
                            dihedral.delta.Add(0);
                            break;

                        case "CNCC":
                        case "CCNC":
                            dihedral.k.Add(0);
                            dihedral.k.Add(2.75f * stiffnessScale);
                            dihedral.k.Add(0);
                            dihedral.k.Add(0.3f * stiffnessScale);
                            dihedral.delta.Add(0);
                            dihedral.delta.Add(180f);
                            dihedral.delta.Add(0);
                            dihedral.delta.Add(0);
                            break;
                        case "HNCH":
                        case "HCNH":
                            dihedral.k.Add(0);
                            dihedral.k.Add(0);
                            dihedral.k.Add(0.11f * stiffnessScale);
                            dihedral.delta.Add(0);
                            dihedral.delta.Add(0);
                            dihedral.delta.Add(0);
                            break;
                        case "HNCC":
                        case "CCNH":
                            dihedral.k.Add(0);
                            dihedral.k.Add(1.4f * stiffnessScale);
                            dihedral.delta.Add(0);
                            dihedral.delta.Add(180f);
                            break;
                        case "CNCH":
                        case "HCNC":
                            dihedral.k.Add(0);
                            dihedral.k.Add(0);
                            dihedral.k.Add(0.1f * stiffnessScale);
                            dihedral.delta.Add(0);
                            dihedral.delta.Add(0);
                            dihedral.delta.Add(0);
                            break;
                        case "HNCO":
                        case "OCNH":
                            dihedral.k.Add(0);
                            dihedral.k.Add(2.5f * stiffnessScale);
                            dihedral.delta.Add(0);
                            dihedral.delta.Add(180f);
                            break;
                        case "CNCO":
                        case "OCNC":
                            dihedral.k.Add(0);
                            dihedral.k.Add(2.75f * stiffnessScale);
                            dihedral.k.Add(0);
                            dihedral.k.Add(0.3f * stiffnessScale);
                            dihedral.delta.Add(0);
                            dihedral.delta.Add(180f);
                            dihedral.delta.Add(0);
                            dihedral.delta.Add(0);
                            break;
                        case "HNCN":
                        case "NCNH":
                            dihedral.k.Add(0);
                            dihedral.k.Add(1.40f * stiffnessScale);
                            dihedral.delta.Add(0);
                            dihedral.delta.Add(180f);
                            break;
                        case "CNCN":
                        case "NCNC":
                            dihedral.k.Add(0);
                            dihedral.k.Add(12.0f * stiffnessScale);
                            dihedral.delta.Add(0);
                            dihedral.delta.Add(180f);
                            break;
                        default:
                            dihedral.k.Add(0);
                            dihedral.delta.Add(0);
                            break;
                    }

                    dihedrals.Add(dihedral);
                }
            }
        }
        this.dihedrals = dihedrals.ToArray();
    }

    private void Organic()
    {
        ReadStructure(Molecule.moleculeFile);
    }

    private void Copper()
    {
        float lat = 0.3615f;
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                for (int k = 0; k < 3; k++)
                {
                    float x = lat * i;
                    float y = lat * j;
                    float z = lat * k;
                    Instantiate(atomPrefab, new Vector3(x, y, z), Quaternion.identity, transform);
                    Instantiate(atomPrefab, new Vector3(x + lat / 2, y + lat / 2, z), Quaternion.identity, transform);
                    Instantiate(atomPrefab, new Vector3(x + lat / 2, y, z + lat / 2), Quaternion.identity, transform);
                    Instantiate(atomPrefab, new Vector3(x, y + lat / 2, z + lat / 2), Quaternion.identity, transform);
                }
            }
        }
        atoms = gameObject.GetComponentsInChildren<Atom>();
        foreach (var atom in atoms)
        {
            atom.mass = cu_eam.mass;
            atom.diameter = lat / Mathf.Sqrt(2)*0.9f;
            atom.color = new Color(0.722f, 0.451f, 0.2f);
        }
    }

    private void Iron() {
        float lat = 0.28553f;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                for (int k = 0; k < 3; k++) {
                    float x = lat * i;
                    float y = lat * j;
                    float z = lat * k;
                    Instantiate(atomPrefab, new Vector3(x, y, z), Quaternion.identity, transform);
                    Instantiate(atomPrefab, new Vector3(x + lat / 2, y + lat / 2, z + lat / 2), Quaternion.identity, transform);
                }
            }
        }
        atoms = gameObject.GetComponentsInChildren<Atom>();
        foreach (var atom in atoms) {
            atom.mass = fe_eam.mass;
            atom.diameter = lat * Mathf.Sqrt(3) * 0.5f * 0.9f;
            atom.color = new Color(0.2627f, 0.2941f, 0.302f);
        }
    }

    // Update is called once per frame
    void Update() {
		pot.CalculateForces();
	}
}
