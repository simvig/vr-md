﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Amber : Potential {

    private MDController controller;

    public Amber(MDController controller)
    {
        this.controller = controller;
    }

    private void BondForce(Bond bond)
    {
        Vector3 u = bond.atom2.transform.position - bond.atom1.transform.position;
        float r = u.magnitude * 10;
        u.Normalize();
        Vector3 f = u * (-2*bond.stiffness*(r-bond.length));
        bond.atom1.force -= f;
        bond.atom2.force += f;
    }

    private void AngleForce(Angle angle)
    {
        Vector3 ba = (angle.atom1.transform.position - angle.atom2.transform.position)*10;
        Vector3 bc = (angle.atom1.transform.position - angle.atom3.transform.position)*10;
        Vector3 cb = -bc;
        Vector3 plane = Vector3.Cross(ba, bc);
        Vector3 pa = Vector3.Cross(ba, plane).normalized;
        Vector3 pc = Vector3.Cross(cb, plane).normalized;
        float a = Vector3.Angle(ba, bc);
        Vector3 fa = -pa * (-2 * angle.stiffness * (a - angle.theta)*Mathf.Deg2Rad / ba.magnitude);
        Vector3 fc = -pc * (-2 * angle.stiffness * (a - angle.theta)*Mathf.Deg2Rad / bc.magnitude);
        Vector3 fb = -fa - fc;
        angle.atom1.force += fb;
        angle.atom2.force += fa;
        angle.atom3.force += fc;
    }

    private void DihedralForce(Dihedral dihedral)
    {
        Vector3 ab = dihedral.atom2.transform.position - dihedral.atom1.transform.position;
        Vector3 bc = dihedral.atom3.transform.position - dihedral.atom2.transform.position;
        Vector3 cd = dihedral.atom4.transform.position - dihedral.atom3.transform.position;
        Vector3 ba = -ab;
        Vector3 cb = -bc;
        Vector3 p1 = Vector3.Cross(ba, bc).normalized;
        Vector3 p2 = Vector3.Cross(cd, cb).normalized;
        Vector3 o = (dihedral.atom2.transform.position + dihedral.atom3.transform.position) / 2;
        Vector3 oc = dihedral.atom3.transform.position - o;
        float theta = Vector3.Angle(p1, p2)*Mathf.Deg2Rad;
        float theta1 = Vector3.Angle(ab, bc) * Mathf.Deg2Rad;
        float theta2 = Vector3.Angle(bc, cd) * Mathf.Deg2Rad;
        float d = 0;
        int n = dihedral.k.Count;
        for(int i = 0; i < n; i++)
        {
            d += dihedral.k[i] * (n+1) * Mathf.Sin((n+1)*(dihedral.delta[i]*Mathf.Deg2Rad - theta));
        }
        Vector3 fa = d/(ab.magnitude*Mathf.Sin(theta1))*p1;
        Vector3 fd = d/ (cd.magnitude * Mathf.Sin(theta2)) * p2;
        Vector3 tc = -(Vector3.Cross(oc,fd)+0.5f*Vector3.Cross(cd,fd)+0.5f*Vector3.Cross(ba,fa));
        Vector3 fc = (1 / oc.sqrMagnitude) * Vector3.Cross(tc, oc);
        Vector3 fb = -fa - fc - fd;
        dihedral.atom1.force += fa;
        dihedral.atom2.force += fb;
        dihedral.atom3.force += fc;
        dihedral.atom4.force += fd;
    }

    private void LJ(Atom atom1, Atom atom2)
    {
        float eps = Mathf.Sqrt(atom1.eps * atom2.eps);
        float sigma = atom1.r + atom2.r;
        Vector3 position = atom1.transform.position - atom2.transform.position;
        float r = position.magnitude*10;
        float d = sigma / r;
        float d3 = d * d * d;
        float d6 = d3 * d3;
        float d12 = d6 * d6;
        Vector3 f = 12 * eps / r * (d12 - d6) * position.normalized;
        atom1.force += f;
        atom2.force -= f;
    }

    public override void CalculateForces()
    {
        foreach(var atom in controller.atoms)
        {
            atom.force = Vector3.zero;
        }
        foreach(var bond in controller.bonds)
        {
           BondForce(bond);
        }
        foreach(var angle in controller.angles)
        {
           AngleForce(angle);
        }
        foreach(var dihedral in controller.dihedrals)
        {
            //DihedralForce(dihedral);
        }
        for(int i = 0; i < controller.atoms.Length; i++)
        {
            for(int j = i+1; j < controller.atoms.Length; j++)
            {
                LJ(controller.atoms[i], controller.atoms[j]);
            }
        }
    }
}
