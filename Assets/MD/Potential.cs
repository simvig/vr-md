﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Potential {

    abstract public void CalculateForces();
}
