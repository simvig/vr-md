﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Angle {

    public Atom atom1;
    public Atom atom2;
    public Atom atom3;

    public float stiffness;
    public float theta;

    public float Radians()
    {
        Vector3 r1 = atom2.transform.position - atom1.transform.position;
        Vector3 r2 = atom3.transform.position - atom1.transform.position;
        return Vector3.Angle(r1, r2) * Mathf.Deg2Rad;
    }
}
