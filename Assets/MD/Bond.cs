﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bond : MonoBehaviour {

    public Atom atom1;
    public Atom atom2;

    public int order;

    public float stiffness;
    public float length;

    public bool slave;

    private LineRenderer lr;

    private Bond second;

    public Camera camera;

	// Use this for initialization
	void Start () {
        lr = GetComponent<LineRenderer>();
        lr.startColor = atom1.color;
        lr.endColor = atom2.color;

        if(order == 2 && !slave)
        {
            lr.startWidth = lr.startWidth / 2;
            lr.endWidth = lr.startWidth;
            second = Instantiate<Bond>(this, transform);
            second.order = 2;
            second.atom1 = atom1;
            second.atom2 = atom2;
            second.slave = true;
        }
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 p1 = atom1.transform.position;
        Vector3 p2 = atom2.transform.position;

        if(order == 2)
        {
            Vector3 r = p1 - p2;
            Vector3 shift = Vector3.Cross(r, camera.transform.forward).normalized;
            if (slave)
            {
                p1 += shift * 0.01f;
                p2 += shift * 0.01f;
            } else
            {
                p1 -= shift * 0.01f;
                p2 -= shift * 0.01f;
            }
        }

        lr.SetPosition(0, p1);
        lr.SetPosition(1, p2);
	}
}
