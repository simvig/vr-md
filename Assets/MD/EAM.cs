﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class EAM : Potential {

    private int nrho;
    private int nr;

    private float drho;
    private float dr;
    private float cutoff;
    private float cutoff2;

    private MDController controller;

    private float[] F;
    private float[] rho;
    private float[] phi;

    public float mass;

    // Use this for initialization
    public EAM(MDController controller, string file) {
        this.controller = controller;

        StreamReader reader = new StreamReader(file);

        //comment lines
        reader.ReadLine();
        reader.ReadLine();
        reader.ReadLine();

        //elements
        reader.ReadLine();

        string[] tokens;
        tokens = reader.ReadLine().Split();
        nrho = int.Parse(tokens[0]);
        drho = float.Parse(tokens[1]);
        nr = int.Parse(tokens[2]);
        dr = float.Parse(tokens[3]);
        cutoff = float.Parse(tokens[4]);
        cutoff2 = cutoff * cutoff;

        tokens = reader.ReadLine().Split();
        mass = float.Parse(tokens[1]);

        F = new float[nrho];
        rho = new float[nr];
        phi = new float[nr];

        for(int i = 0; i < nrho; i++)
        {
            F[i] = float.Parse(reader.ReadLine());
        }
        for(int i = 0; i < nr; i++)
        {
            rho[i] = float.Parse(reader.ReadLine());
        }
        for(int i = 0; i < nr; i++)
        {
            phi[i] = float.Parse(reader.ReadLine());
        }
	}

    private float Distance2(Vector3 coords1, Vector3 coords2)
    {
        float dx = coords1.x - coords2.x;
        float dy = coords1.y - coords2.y;
        float dz = coords1.z - coords2.z;

        return (dx * dx + dy * dy + dz * dz)*100;
    }

    float PhiInterp(float r)
    {
        float distance = r / dr;
        int place = (int)distance;
        distance -= place;
        if(place >= phi.Length)
        {
            Debug.Log("WARNING: r=" + r + " outside of phi boundaries");
            return 0;
        }
        return Mathf.Lerp(phi[place], phi[place + 1], distance);
    }

    float RhoInterp(float r)
    {
        float distance = r / dr;
        int place = (int)distance;
        distance -= place;
        if(place >= rho.Length)
        {
            Debug.Log("WARNING: r=" + r + " outside of rho boundaries");
            return 0;
        }
        return Mathf.Lerp(rho[place], rho[place + 1], distance);
    }

    float FInterp(float rho)
    {
        float distance = rho / drho;
        int place = (int)distance;
        distance -= place;
        if(place >= F.Length)
        {
            Debug.Log("WARNING: rho=" + rho + " outside of F boundaries");
            return 0;
        }
        return Mathf.Lerp(F[place], F[place + 1], distance);
    }

    private float Energy(int atomNum, Vector3 position)
    {
        Atom atom = controller.atoms[atomNum];
        float energy = 0.0f;
        float sumRho = 0.0f;
        for(int i = 0; i < controller.atoms.Length; i++)
        {
            if (i == atomNum) continue;
            Atom other = controller.atoms[i];
            float d2 = Distance2(position, other.transform.position);
            if (d2 > cutoff2) continue;
            float d = Mathf.Sqrt(d2);
            sumRho += RhoInterp(d);
            energy += PhiInterp(d);
        }
        energy += FInterp(sumRho);
        return energy;
    }

    public override void CalculateForces()
    {
        for (int i = 0; i < controller.atoms.Length; i++)
        {
            Vector3 pos = controller.atoms[i].transform.position;

            pos.x += 0.01f;
            float dex = Energy(i, pos);
            pos.x -= 0.01f * 2;
            dex -= Energy(i, pos);
            pos.x += 0.01f;

            pos.y += 0.01f;
            float dey = Energy(i, pos);
            pos.y -= 0.01f * 2;
            dey -= Energy(i, pos);
            pos.y += 0.01f;

            pos.z += 0.01f;
            float dez = Energy(i, pos);
            pos.z -= 0.01f * 2;
            dez -= Energy(i, pos);

            float fx = -dex / 0.01f / 2;
            float fy = -dey / 0.01f / 2;
            float fz = -dez / 0.01f / 2;

            Vector3 force = new Vector3(fx, fy, fz);
            controller.atoms[i].force = force;
        }
    }
}
