﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dihedral {

    public Atom atom1;
    public Atom atom2;
    public Atom atom3;
    public Atom atom4;

    public List<float> k = new List<float>();
    public List<float> delta = new List<float>();
}
