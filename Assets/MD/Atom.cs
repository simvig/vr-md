﻿using System.Collections;
using System.Collections.Generic;
using OVRTouchSample;
using UnityEngine;
using System.Collections.Generic;

public class Atom : MonoBehaviour {

	public float mass;
    public float diameter;
    public float eps;
    public float r;
    public string type;
    public Color color;
    public List<int> bonds = new List<int>();
	public Vector3 force = Vector3.zero;

	private Vector3 a = Vector3.zero;

	private ColorGrabbable grab;

    private Parameters parameters;
    private Vector3 previousPosition;

    private Vector3 initialPosition;
    private MDController controller;

	// Use this for initialization
	void Start() {
        controller = FindObjectOfType<MDController>();
        parameters = FindObjectOfType<Parameters>();
		grab = GetComponent<ColorGrabbable>();
        grab.m_color = color;
        grab.SetColor(color);
        Vector3 scale = new Vector3(diameter, diameter, diameter);
        transform.localScale = scale;
        previousPosition = transform.position;
        initialPosition = transform.position;
	}

    // Update is called once per frame
    void Update() {
        if (!grab.isGrabbed)
        {
            a = force * (1 / mass);
            Vector3 newPosition = 2 * transform.position - previousPosition + a * Time.deltaTime * Time.deltaTime;
            /*
            while (newPosition.x > parameters.box.x) newPosition.x -= 2 * parameters.box.x;
            while (newPosition.x < -parameters.box.x) newPosition.x += 2 * parameters.box.x;
            while (newPosition.y > parameters.box.y) newPosition.y -= 2 * parameters.box.y;
            while (newPosition.y < -parameters.box.y) newPosition.y += 2 * parameters.box.y;
            while (newPosition.z > parameters.box.z) newPosition.z -= 2 * parameters.box.z;
            while (newPosition.z < -parameters.box.z) newPosition.z += 2 * parameters.box.z;
            */
            Vector3 shift = newPosition - transform.position;
            float distance = shift.magnitude;
            if(distance > 2.0f / 90)
{
    shift = shift * (2.0f / 90 / distance);
}
            if (controller.damping && distance > 0.1f / 90)
            {
                shift = shift * 0.9f;
            }
            newPosition = transform.position + shift;
            if (newPosition.x > parameters.box.x - diameter / 2 || newPosition.x < -parameters.box.x + diameter / 2) newPosition.x = (previousPosition.x + transform.position.x) / 2;
            if (newPosition.y > parameters.box.y - diameter / 2 || newPosition.y < -parameters.box.y + diameter / 2) newPosition.y = (previousPosition.y + transform.position.y) / 2;
            if (newPosition.z > parameters.box.z - diameter / 2 || newPosition.z < -parameters.box.z + diameter / 2) newPosition.z = (previousPosition.z + transform.position.z) / 2;
            previousPosition = transform.position;
            transform.position = newPosition;
        }
        else
        {
            previousPosition = transform.position;
        }
	}
}
