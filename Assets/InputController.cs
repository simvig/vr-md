﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InputController : MonoBehaviour {

    private MDController mdController;

	// Use this for initialization
	void Start () {
        mdController = FindObjectOfType<MDController>();
	}

    // Update is called once per frame
    void Update () {
        if (OVRInput.GetDown(OVRInput.RawButton.A))
        {
            MDController.type = "Organic";
            Molecule.moleculeFile = "Assets/Resources/glucose.mol";
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        if (OVRInput.GetDown(OVRInput.RawButton.B))
        {
            MDController.type = "Organic";
            Molecule.moleculeFile = "Assets/Resources/caffeine.mol";
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        if (OVRInput.GetDown(OVRInput.RawButton.X))
        {
            MDController.type = "Organic";
            Molecule.moleculeFile = "Assets/Resources/aspirin.mol";
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        if (OVRInput.GetDown(OVRInput.RawButton.Y))
        {
            MDController.type = "Organic";
            Molecule.moleculeFile = "Assets/Resources/capsaicin.mol";
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        if(OVRInput.GetDown(OVRInput.RawButton.LThumbstick)) {
            MDController.type = "Copper";
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        if(OVRInput.GetDown(OVRInput.RawButton.RThumbstick)) {
            MDController.type = "Iron";
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        if (OVRInput.Get(OVRInput.RawButton.LIndexTrigger) || OVRInput.Get(OVRInput.RawButton.RIndexTrigger))
        {
            mdController.damping = true;
        } else
        {
            mdController.damping = false;
        }
	}
}
